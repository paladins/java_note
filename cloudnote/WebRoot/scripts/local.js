﻿/**********公共函数**********/
//格式化字符串，转义<和>
function formate_name(e){
	e=e.replace(/</g,'&lt;');
	e=e.replace(/>/g,'&gt;');
	return e;
}
//去掉空格
function check_null(s){
	s=s.replace(/ /g,'');
	s=s.length;
	return s;
}


/**********HTML初始化时直接调用的函数**********/
//获取笔记本列表,edit.html初始化时调用
function get_nb_list(){
	loadNormalNoteBook();
}

//获取特殊笔记本列表,edit.html初始化时调用
function get_spnb_list(){
	loadSpecialNoteBook();
}

//获取活动列表,activity.html初始化时调用
function activity_list(){
	getActivityList();
}

//获取活动页面参加活动笔记列表,activity_detail.html初始化时调用
function get_activity_list(){
	var param=window.location.hash;
	global_ac_id=param.replace(/#/,'');
	getNoteActivitys();
}



/**********HTML初始化后为其按钮绑定函数**********/
//注册事件
$(function(){
	//显示用户名
	$(".profile-username").text(getCookie("userName"));
	
	//----关闭，取消
	$(document).on("click", ".close,.cancle", function() {
		//清空弹出页面中输入的内容
		$('#input_notebook,#input_note').val('');
		//隐藏弹出页面
        $('.modal.fade.in').hide();
        //隐藏背景图片
        $('.opacity_bg').hide();
    }),
	
    
    /***********笔记本模块************/
	//----单击笔记本,查询笔记
	$(document).on("click", "#pc_part_1 li", function() {
		//显示出笔记和编辑笔记的div
		$('#pc_part_2,#pc_part_3').show();
		//清空笔记列表
		$('#pc_part_2 ul').empty();
		//清空笔记标题和内容
		$("#input_note_title").val("");
		um.setContent("");
		//回收站笔记、收藏笔记、活动笔记、
		//搜索笔记以及预览笔记隐藏
		$('#pc_part_4,#pc_part_5,#pc_part_6,#pc_part_7,#pc_part_8').hide();
		//回收站、收藏、活动按钮的选中样式移除
		$('#rollback_button,#like_button,#action_button').removeClass('clicked');
		
		//siblings是获取当前元素的所有兄弟元素，
		//但不包括自己，其中this指代当前点击的li。
		//$(this).siblings('li')是选中其他的li。
		//checked是选中的样式
		$(this).siblings('li').children('a').removeClass('checked');
		//当前选中li中的a元素，设置为checked。
		//结果是只有选中的li是checked样式
		$(this).children('a').addClass('checked');
		
		//获取笔记本下的笔记列表
		var checked_li = $("#first_side_right .checked").parent();
		var notebook = checked_li.data("notebook");
		var noteBookId = notebook.cn_notebook_id;
		getNormalNoteList(noteBookId);
    }),
    
	//----打开创建笔记本界面
	$(document).on("click", "#add_notebook", function() {
		//在can这个div里，显示alert_notebook.html的内容
		//并且在显示之后调用function
		$('#can').load('./alert/alert_notebook.html', function(){
			$('#input_notebook').focus();
		});
		//弹出半透明的背景图
		$('.opacity_bg').show();
    }),
    
	//----创建笔记本
	$(document).on("click", "#modalBasic .btn.btn-primary.sure", function() {
		//获取用户输入的笔记本名称
		var noteBookName = $("#input_notebook").val();
		if(noteBookName) {
			//调用notebook.js中的addNoteBook
			addNoteBook(noteBookName);
		}
    }),
    
	//----双击,打开修改笔记本界面
    //获取笔记本列表下，下标大于0的li，
    //即获取除了默认笔记本之外的li，
    //给这些li注册双击事件
	$(document).on("dblclick", "#pc_part_1 li:gt(0)", function() {
		//弹出修改名称的页面
		$('#can').load('./alert/alert_rename.html',function(){
			//弹出页面的文本框赋予焦点
			$('#input_notebook_rename').focus();
		});
		//显示半透明的背景图
		$('.opacity_bg').show();
    }),
    
    //修改笔记本
	$(document).on("click",'#modalBasic_4 .sure',function() {
		//获取新名称
		var newName = $("#input_notebook_rename").val();
		//获取选中的li，取得绑定的笔记本数据
		var checked_li = $("#first_side_right .checked").parent();
		var notebook = checked_li.data("notebook");
		//校验新名称非空，新名称不能与旧名称相同
		if(newName && newName!=notebook.cn_notebook_name) {
			//调用notebook.js中的方法
			updateNoteBook(newName,notebook,checked_li);
		}
	});
    
	//----打开删除笔记本界面
	$(document).on("click", "#first_side_right .btn_delete", function() {
		$('#can').load('./alert/alert_delete_notebook.html');
		$('.opacity_bg').show();
    }),
	
    //----删除笔记本
	$(document).on('click','#modalBasic_6 .btn.btn-primary.sure',function(){
		//删除
		var checked_li = $("#first_side_right .checked").parent();
		deleteNoteBook(checked_li);
	});
    
	
	
	/***********笔记模块************/
	//----点击笔记
	$(document).on("click", "#pc_part_2 li", function() {
		//设置当前笔记处于选中状态
		$(this).siblings('li').children('a').removeClass('checked');
		$(this).children('a').addClass('checked');
		//查询笔记，将标题和内容写入笔记的编辑框中
		getNoteDetail();
    }),
    
	//----打开创建笔记界面
	$(document).on("click", "#add_note", function() {
		$('#can').load('./alert/alert_note.html',function() {
			$('#input_note').focus();
		});
		$('.opacity_bg').show();
    }),
    
	//----创建笔记
	$(document).on("click", "#modalBasic_2 .btn.btn-primary.sure", function() {
		//获取当前选中的笔记本
		var checked_li = $("#first_side_right .checked").parent();
		var notebook = checked_li.data("notebook");
		//获取输入的笔记名
		var title = $('#input_note').val();
		if(title) {
			//保存
			createNormalNote(notebook.cn_notebook_id,title);
		}
    }),
    
    //----保存笔记内容
    $(document).on("click","#save_note", function() {
		//修改
		updateNormalNote();
    }),
    
    //----点击笔记下拉按钮
	$(document).on("click", ".btn_slide_down", function() {
		$(this).parents('li').children('.note_menu').addClass('note_menu_show').mouseleave(function(){
			$(this).removeClass('note_menu_show');
		});
    }),
    
    //----打开删除笔记界面
	$(document).on("click", "#second_side_right .btn_delete", function() {
		$('#can').load('./alert/alert_delete_note.html');
		$('.opacity_bg').show();
    }),
    
	//----确认删除
	$(document).on('click','#modalBasic_7 .btn.btn-primary.sure', function() {
		//获取选中的笔记ID
		var checked_li = $("#second_side_right .checked").parent();
		var note = checked_li.data("note");
		//获取回收站笔记本ID
		var notebook = $("#rollback_button").data("notebook");
		moveNote(note.cn_note_id,notebook.cn_notebook_id,checked_li);
	});
    
	//----打开移动笔记界面
	$(document).on("click", "#second_side_right .btn_move", function() {
		$('#can').load('./alert/alert_move.html',function(){
			// 获取笔记本列表
			setNoteBookToSelect();
			$('#moveSelect').focus();
		});
		$('.opacity_bg').show();
    }),
    
	//----确认移动
	$(document).on('click','#modalBasic_11 .btn.btn-primary.sure',function(){
		//获得选中的笔记
		var checked_li = $("#second_side_right .checked").parent();
		var note = checked_li.data("note");
		//获得用户选择的要移动到的笔记本
		var noteBookId = $('#moveSelect').val();
		//如果用户选择了笔记本，并且与之前笔记本不同
		if(noteBookId && noteBookId!=note.cn_notebook_id) {
			moveNote(note.cn_note_id,noteBookId,checked_li);
		}
	});
    
	//----分享笔记
	$(document).on("click", "#second_side_right .btn_share", function() {
		//将当前点击的分享按钮淡出
		$(this).fadeOut(600);
		//分享笔记
		createShareNote();
    }),
    
    
    /***********回收站模块************/
	//----点击回收站按钮
	$(document).on("click", "#rollback_button", function() {
		$('#pc_part_2,#pc_part_3,#pc_part_6,#pc_part_7,#pc_part_8').hide();
		$('#pc_part_4,#pc_part_5').show();
		$('#first_side_right li a').removeClass('checked');
		$('#like_button,#action_button').removeClass('clicked');
		$(this).addClass('clicked');
		//每次加载前先清空所有li
		//$('#pc_part_4 ul').empty();
		getRecycleNoteList();
    }),
    
	//----点击回收站笔记
	$(document).on("click", "#pc_part_4 li", function() {
		$(this).siblings('li').children('a').removeClass('checked');
		$(this).children('a').addClass('checked');
		getRecycleNoteDetail();
    }),
    
	//----点击回收站恢复按钮
	$(document).on("click", "#four_side_right .btn_replay", function() {
		$('#can').load('./alert/alert_replay.html',function(){
			setNoteBookToSelect();
			$('#replaySelect').focus();
		});
		$('.opacity_bg').show();
    }),
    
	//----确认恢复
	$(document).on('click','#modalBasic_3 .btn.btn-primary.sure', function(){
		moveNote();
	});

	//----点击回收站删除按钮
	$(document).on("click", "#four_side_right .btn_delete", function() {
		$('#can').load('./alert/alert_delete_rollback.html');
		$('.opacity_bg').show();
    }),
    
	//----确认删除
	$(document).on('click','#modalBasic_10 .btn.sure', function() {
		deleteRecycleNote();
	});
    
	
	/***********搜索笔记模块************/
	//----搜索笔记
	$(document).on("keyup", "body", function(e) {
		//当搜索框有焦点，且键入了Enter
		//即在搜索框中敲了回车，触发此代码
		//按回车触发的查询是查询第一页
		if($('#search_note').is(':focus')&&(e.keyCode==108||e.keyCode==13)){
			//获取搜索框中的内容
			var m=$('#search_note').val();
			//去掉空格
			var n=m.replace(/ /g,'');
			if(n){
				//每次搜索前，清空列表
				$('#sixth_side_right ul').empty();
				//将当前页1绑定到[更多]按钮上
				$("#more_note").data("currentPage",1);
				//搜索
				getShareNoteList(n,1);
			}
		}
    }),
    
	//----更多搜索笔记
	$(document).on("click", "#more_note", function() {
		//获取搜索框中的内容
		var m=$('#search_note').val();
		//去掉空格
		var n=m.replace(/ /g,'');
		if(n){
			//取得当前页
			var p = $("#more_note").data("currentPage");
			//将当前页1绑定到[更多]按钮上
			$("#more_note").data("currentPage",p+1);
			//搜索
			getShareNoteList(n,p+1);
		}
    }),
    
	//----点击搜索笔记
	$(document).on("click", "#sixth_side_right li", function() {
		//设置当前的li为选中状态
		$(this).siblings('li').children('a').removeClass('checked');
		$(this).children('a').addClass('checked');
		//预览
		getShareNoteDetail();
    }),
    
	//----收藏搜索笔记
	$(document).on("click", "#pc_part_6 .btn_like", function() {
		//弹出收藏页面
		$('#can').load('./alert/alert_like.html', function(){
			//在弹出页面的同时，执行了如下代码，
			//给弹出页面上的收藏按钮注册单击事件
			$('#modalBasic_5 .btn.btn-primary.sure').click(function(){
				//收藏
				likeShareNote();
			});
		});
		//弹出背景图片
		$('.opacity_bg').show();
    }),
    
    
    /***********注册收藏笔记相关操作************/
	//----点击收藏按钮
	$(document).on("click", "#like_button", function() {
		//设置div的显示
		$('#pc_part_2,#pc_part_3,#pc_part_4,#pc_part_6,#pc_part_8').hide();
		$('#pc_part_7,#pc_part_5').show();
		//将笔记本的选中状态移除
		$('#first_side_right li a').removeClass('checked');
		//将回收站、活动按钮选中样式移除
		$('#rollback_button,#action_button').removeClass('clicked');
		//将当前点击的收藏按钮设置为选中
		$(this).addClass('clicked');
		//每次加载前先清空收藏列表
		$('#pc_part_7 ul').empty();
		//获取收藏按钮绑定的数据
		var notebook = $(this).data("notebook");
		getLikeNoteList(notebook.cn_notebook_id);
    }),
	
	//----点击收藏笔记
	$(document).on("click", "#pc_part_7 li", function() {
		$(this).siblings('li').children('a').removeClass('checked');
		$(this).children('a').addClass('checked');
		getLikeNoteDetail();
    }),
    
	//----点击取消收藏(删除)
	$(document).on("click", "#pc_part_7 li .btn_delete", function() {
		$('#can').load('./alert/alert_delete_like.html');
		$('.opacity_bg').show();
    }),
    
	//----确认取消
	$(document).on('click','#modalBasic_9 .btn.btn-primary.sure', function(){
		//获取选中的收藏笔记
		var checked_li = $("#seventh_side_right .checked").parent();
		var note = checked_li.data("note");
		//获取回收站笔记本
		var notebook = $("#rollback_button").data("notebook");
		moveNote(note.cn_note_id,notebook.cn_notebook_id,checked_li);
	});
    
	
	/***********参加活动笔记模块************/
	//----点击参加活动笔记按钮
	$(document).on("click", "#action_button", function() {
		$('#pc_part_2,#pc_part_3,#pc_part_6,#pc_part_7,#pc_part_4').hide();
		$('#pc_part_8,#pc_part_5').show();
		$('#first_side_right li a').removeClass('checked');
		$('#rollback_button,#like_button').removeClass('clicked');
		$(this).addClass('clicked');
		//$("#eighth_side_right ul").empty();
		getNoteActivityList();
    }),
    
    //----点击参加活动笔记
	$(document).on("click", "#pc_part_8 li", function() {
		$(this).siblings('li').children('a').removeClass('checked');
		$(this).children('a').addClass('checked');
		getActivityNoteDetail();
    }),
	
    
    
    
    
    /***********活动模块************/
	//----更多活动笔记
	$(document).on("click", "#more_activity_note", function() {
		getNoteActivitys();
    });
	
	//----点击笔记(活动页面)
	$(document).on("click", "#action_part_1 li", function() {
		$('#rollback_button').removeClass('clicked');
		$(this).siblings('li').children('a').removeClass('checked');
		$(this).children('a').addClass('checked');
		$("#content_body").empty();
		getNoteActivityDetail();
    }),
	
	//----点击参加活动（活动页面）
	$(document).on("click", "#join_action", function() {
		$('#modalBasic_15,.opacity_bg').show();
		//$('#select_notebook ul').empty();
		//$('#select_note ul').empty();
		getSelectNoteBook();
    }),
    
	//----准备选择参加活动笔记（活动页面）
	$(document).on("click", "#select_notebook li", function() {
		$(this).siblings('li').children('a').removeClass('checked');
		$(this).children('a').addClass('checked');
		//$('#select_note ul').empty();
		getSelectNoteList();
    }),
    
	//----选择笔记（活动页面）
	$(document).on("click", "#select_note li", function() {
		$(this).siblings('li').children('a').removeClass('checked');
		$(this).children('a').addClass('checked');
    }),

	//----确认选择的笔记（活动页面）
	$(document).on("click", "#modalBasic_15 .btn.btn-primary.sure", function() {
		//var get_notename=$('#select_note li a.checked').text();
		//$('.close,.cancle').trigger('click');
		createNoteActivity();
    }),
    
	//----点击收藏（活动页面）
	$(document).on('click',"#first_action .btn_like", function() {
		likeActivityNote();
    }),
	
	//----顶笔记（活动页面）
	$(document).on("click", "#first_action .btn_up", function() {
		up();
    }),
    
	//----踩笔记（活动页面）
	$(document).on("click", "#first_action .btn_down", function() {
		down();
    });
	
});
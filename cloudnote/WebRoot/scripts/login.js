/**
 * 登录页面初始化后，绑定函数。
 */
$(function(){
	//注册
	$("#regist_button").click(function(){
		//调用业务方法
		register();
	});
	
	//登录
	$("#login").click(function(){
		login();
	});
	
	//登出
	$("#logout").click(function(){
		logout();
	});
	
	//修改密码
	$("#changePassword").click(function(){
		changepwd();
	})
	
});

//注册
function register() {
	console.log("进入注册方法");
	//客户端验证
	//取值
	var userName = $("#regist_username").val();
	var nickName = $("#nickname").val();
	var password = $("#regist_password").val();
	var finalPassword = $("#final_password").val();
	//校验账号，3-20位字母、数字、下划线
	var reg = /^\w{3,20}$/;
	if(!reg.test(userName)) {
		//如果用户名不满足要求
		$("#warning_1 span").text("请输入3-20位字母、数字、下划线的组合.");
		$("#warning_1").show();
		return;
	} else {
		$("#warning_1").hide();
	}
	//校验密码，不能小于6位
	if(password.length<6) {
		$("#warning_2 span").text("密码长度不能小于6位.");
		$("#warning_2").show();
		return;
	} else {
		$("#warning_2").hide();
	}
	//校验确认密码
	if(password!=finalPassword) {
		$("#warning_3 span").text("2次输入的密码不一致.");
		$("#warning_3").show();
		return;
	} else {
		$("#warning_3").hide();
	}
	
	console.log("准备调用注册方法");
	//注册
	$.post(
			basePath + "/login/register.do",
			{"cn_user_name":userName,
				"cn_user_desc":nickName,
				"cn_user_password":password},
			function(result) {
				//{"success":true,"message":"","data":true}
				console.log("进入回调函数");
				if(result.success) {
					//代码执行成功，没报错
					//判断用户名是否存在
					if(result.data) {
						//注册成功，回到登录页
						alert("注册成功.");
						$("#zc").attr("class","sig sig_out");
						$("#dl").attr("class","log log_in");
					} else {
						//注册失败
						$("#warning_1 span").text("用户名已存在.");
						$("#warning_1").show();
					}
				} else {
					//代码执行失败，给予错误提示
					alert(result.message);
				}
			}
	);
}

//登陆
function login() {
	console.log("进入login");
	//客户端验证
	//判断用户名是否为空
	var userName = $("#count").val();
	console.log(userName);
	if(!userName) {
		alert("请输入用户名.");
		return;
	}
	//判断密码是否为空
	var password = $("#password").val();
	if(!password) {
		alert("请输入密码.");
		return;
	}
	console.log("准备发送异步请求");
	//校验用户名、密码是否正确
	$.post(
		basePath + "/login/login.do",
		{"userName":userName,"password":password},
		function(result) {
			console.log("进入回调函数");
			if(result.success) {
				//程序执行正确
				var map = result.data;
				if(map.flag==0) {
					//登录成功，跳转至edit.html
					addCookie("userId",map.userId,10);
					addCookie("userName",map.userName,10);
					location.href="edit.html";
				} else if (map.flag==1) {
					//用户名错误
					alert("用户名错误.");
				} else {
					alert("密码错误.");
				}
			} else {
				//程序执行报错
				alert(result.message);
			}
		}
	);
}

/**
 * 退出登录
 */
function logout(){
	//注销session
	$.post(
		basePath+"/login/logout.do",
		{},
		function(result) {
			if(result.success) {
				//程序执行成功
				location.href="login.html";
			} else {
				//程序执行失败
				alert(result.message);
			}
		}
	);
}

/**
 * 修改密码
 */
function changepwd(){
	//获取数据
	var oldPassword = $("#last_password").val();
	var newPassword = $("#new_password").val();
	var finalPassword = $("#final_password").val();
	
	//客户端验证
	if(oldPassword.length<6) {
		$("#warning_1 span").text("密码长度不能小于6位.");
		$("#warning_1").show();
		return;
	} else {
		$("#warning_1").hide();
	}
	if(newPassword.length<6) {
		$("#warning_2 span").text("密码长度不能小于6位.");
		$("#warning_2").show();
		return;
	} else {
		$("#warning_2").hide();
	}
	if(newPassword!=finalPassword) {
		$("#warning_3 span").text("2次输入的密码不一致.");
		$("#warning_3").show();
		return;
	} else {
		$("#warning_3").hide();
	}
	
	//修改密码
	$.post(
		basePath+"/login/changePassword.do",
		{"oldPassword":oldPassword,
			"newPassword":newPassword},
		function(result) {
			console.log("进入回调函数");
			if(result.success) {
				//程序正确执行
				if(result.data) {
					//密码修改成功，强制退出，重新登录
					logout();
				} else {
					//密码修改失败，原密码错误
					$("#warning_1 span").text("原密码错误.");
					$("#warning_1").show();
				}
			} else {
				//程序执行报错
				alert(result.message);
			}
		}
	);
}



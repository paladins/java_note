/***
 * 加载普通笔记
 */
function getNormalNoteList(noteBookId){
	$.post(
		basePath+"/note/findNote.do",
		{"noteBookId":noteBookId},
		function(result) {
			if(result.success) {
				//获取笔记集合
				var list = result.data;
				//遍历笔记
				$(list).each(function(){
					var li = '<li class="online"><a><i class="fa fa-file-text-o" title="online" rel="tooltip-bottom"></i> '+this.cn_note_title+'<button type="button" class="btn btn-default btn-xs btn_position btn_slide_down"><i class="fa fa-chevron-down"></i></button></a><div class="note_menu" tabindex="-1"><dl><dt><button type="button" class="btn btn-default btn-xs btn_move" title="移动至..."><i class="fa fa-random"></i></button></dt><dt><button type="button" class="btn btn-default btn-xs btn_share" title="分享"><i class="fa fa-sitemap"></i></button></dt><dt><button type="button" class="btn btn-default btn-xs btn_delete" title="删除"><i class="fa fa-times"></i></button></dt></dl></div></li>';					
					$("#second_side_right ul").append(li);
					$("#second_side_right ul li:last").data("note",this);
				});
			} else {
				alert(result.message);
			}
		}
	);
}

/***
 * 查询普通笔记内容
 */
function getNoteDetail(){
	//取得选中笔记li绑定的数据
	var checked_li = $("#second_side_right .checked").parent();
	var note = checked_li.data("note");
	//设置标题
	$("#input_note_title").val(note.cn_note_title);
	//设置内容
	um.setContent(note.cn_note_body==null?"":note.cn_note_body);
}

/***
 * 创建普通笔记
 */
function createNormalNote(noteBookId, noteTitle){
	$.post(
		basePath+"/note/add.do",
		{"noteBookId":noteBookId,
			"noteTitle":noteTitle},
		function(result) {
			if(result.success) {
				var note = result.data;
				//在笔记列表中追加笔记li
				var li = '<li class="online"><a><i class="fa fa-file-text-o" title="online" rel="tooltip-bottom"></i> '+note.cn_note_title+'<button type="button" class="btn btn-default btn-xs btn_position btn_slide_down"><i class="fa fa-chevron-down"></i></button></a><div class="note_menu" tabindex="-1"><dl><dt><button type="button" class="btn btn-default btn-xs btn_move" title="移动至..."><i class="fa fa-random"></i></button></dt><dt><button type="button" class="btn btn-default btn-xs btn_share" title="分享"><i class="fa fa-sitemap"></i></button></dt><dt><button type="button" class="btn btn-default btn-xs btn_delete" title="删除"><i class="fa fa-times"></i></button></dt></dl></div></li>';
				$("#second_side_right ul").append(li);
				//给笔记li绑定笔记数据对象
				$("#second_side_right ul li:last").data("note",note);
				//关闭弹出框
				$(".close,.cancle").trigger("click");
				//选中新增的笔记
				$("#second_side_right ul li:last").trigger("click");
			} else {
				alert(result.message);
			}
		}
	);
}

/***
 * 更新普通笔记
 */
function updateNormalNote(){
	//获取选中笔记li的绑定数据
	var checked_li = $("#second_side_right .checked").parent();
	var note = checked_li.data("note");
	//获取修改后的标题和内容
	var title = $("#input_note_title").val();
	var body = um.getContent();
	//更新笔记对象
	note.cn_note_title=title;
	note.cn_note_body=body;
	//修改笔记
	$.post(
		basePath+"/note/update.do",
		note,
		function(result) {
			if(result.success) {
				//修改笔记li中的标题
				$("#second_side_right .checked").html('<i class="fa fa-file-text-o" title="online" rel="tooltip-bottom"></i> '+note.cn_note_title+'<button type="button" class="btn btn-default btn-xs btn_position btn_slide_down"><i class="fa fa-chevron-down"></i></button>');
				//重新绑定数据
				var checked_li = $("#second_side_right .checked").parent();
				checked_li.data("note",note);
				//提示
				$("footer strong").text("保存成功");
				//淡入
				$("footer div").fadeIn(200);
				//淡出
				$("footer div").fadeOut(3000);
				//setTimeout(function(){
				//	$("footer div").hide();
				//},2000);
			} else {
				alert(result.message);
			}
		}
	);
}

/***
 * 移动笔记
 */
function moveNote(noteId,noteBookId,checked_li){
	$.post(
		basePath+"/note/move.do",
		{"noteId":noteId,
			"noteBookId":noteBookId},
		function(result) {
			if(result.success) {
				//移除当前笔记
				//$("#second_side_right .checked").parent().remove();
				checked_li.remove();
				//关闭弹出框
				$(".close,.cancle").trigger("click");
			} else {
				alert(result.message);
			}
		}
	);
}

/***
 * 分享笔记
 */
function createShareNote(){
	//获取当前选中的笔记数据
	var checked_li = $("#second_side_right .checked").parent();
	var note = checked_li.data("note");
	$.post(
		basePath+"/note/share.do",
		note,
		function(result) {
			if(result.success) {
				//给予提示
				$("footer div strong").text("分享成功").parent().fadeIn(200);
				$("footer div").fadeOut(3000);
			} else {
				alert(result.message);
			}
		}
	);
}

/***
 * 查询回收站笔记列表
 */
function getRecycleNoteList(){
	alert("查询回收站笔记列表");
}

/***
 * 查看回收站笔记内容
 */
function getRecycleNoteDetail() {
	console.log("查看回收站笔记内容");
}

/***
 * 删除回收站笔记
 */
function deleteRecycleNote(){
	alert("删除回收站笔记");
}

/***
 * 搜索分享笔记列表
 */
function getShareNoteList(searchKey,currentPage){
	//设置显示和隐藏的div
	$("#pc_part_1,#pc_part_5,#pc_part_6").show();
	$("#pc_part_2,#pc_part_3,#pc_part_4,#pc_part_7,#pc_part_8").hide();
	$('#rollback_button,#like_button,#action_button').removeClass('clicked');
	//搜索
	$.post(
		basePath+"/share/search.do",
		{"searchKey":searchKey,
			"currentPage":currentPage},
		function(result) {
			if(result.success) {
				var list = result.data;
				$(list).each(function(){
					var li = '<li class="online"><a href="#"><i class="fa fa-file-text-o" title="online" rel="tooltip-bottom"></i> '+this.cn_share_title+'<button type="button" class="btn btn-default btn-xs btn_position btn_like"><i class="fa fa-star-o"></i></button><div class="time"></div></a></li>';					
					$("#sixth_side_right ul").append(li);
					$("#sixth_side_right ul li:last").data("share",this);
				});
			} else {
				alert(result.message);
			}
		}
	);
}

/***
 * 查询分享笔记内容
 */
function getShareNoteDetail(){
	//获取当前选中的分享笔记
	var checked_li = $("#sixth_side_right .checked").parent();
	var share = checked_li.data("share");
	//设置预览数据
	$("#noput_note_title").text(share.cn_share_title);
	$("#noput_note_body").html(share.cn_share_body);
}

/***
 * 收藏分享笔记
 */
function likeShareNote(){
	//获取选中的搜索笔记
	var checked_li = $("#sixth_side_right .checked").parent();
	var share = checked_li.data("share");
	//收藏
	$.post(
		basePath+"/share/like.do",
		share,
		function(result) {
			if(result.success) {
				$("#pc_part_6 .checked .btn_like").fadeOut(300);
				//关闭提示框
				$(".close,.cancle").trigger("click");
				//提示
				$("footer strong").text("收藏成功.");
				$("footer div").fadeIn(200);
				$("footer div").fadeOut(3000);
			} else {
				alert(result.message);
			}
		}
	);
}

/***
 * 加载收藏笔记
 */
function getLikeNoteList(like_notebook_id){
	$.post(
			basePath+"/note/findNote.do",
			{"noteBookId":like_notebook_id},
			function(result) {
				if(result.success) {
					var list = result.data;
					$(list).each(function(){
						var li = '<li class="idle"><a ><i class="fa fa-file-text-o" title="online" rel="tooltip-bottom"></i> '+this.cn_note_title+'<button type="button" class="btn btn-default btn-xs btn_position btn_delete"><i class="fa fa-times"></i></button></a></li>';
						$("#seventh_side_right ul").append(li);
						$("#seventh_side_right ul li:last").data("note",this);
					});
				} else {
					alert(result.message);
				}
			}
	);
}

/***
 * 查看收藏笔记内容
 */
function getLikeNoteDetail() {
	//获取选中的笔记
	var checked_li = $("#seventh_side_right .checked").parent();
	var note = checked_li.data("note");
	//设置预览内容
	$("#noput_note_title").text(note.cn_note_title);
	$("#noput_note_body").html(note.cn_note_body);
}

/***
 * 加载本用户参加活动笔记列表
 */
function getNoteActivityList(noteBookId){
	alert("加载本用户参加活动笔记列表");
}

/***
 * 查询参加活动的笔记内容
 */
function getActivityNoteDetail(noteId) {
	console.log("查询参加活动的笔记内容");
}
/***
 * 加载普通笔记本
 */
function loadNormalNoteBook(){
	console.log("into loadNormalNoteBook");
	$.post(
		basePath+"/notebook/findNormal.do",
		{},
		function(result) {
			console.log("into callback function");
			if(result.success) {
				//程序执行成功
				//循环查询到的笔记本，插入到ul中
				var list = result.data;
				$(list).each(function(){
					console.log(this.cn_notebook_name);
					//声明要插入的li对象，
					//包含了本次循环的笔记本名
					var li = '<li class="online"><a class="unchecked"><i class="fa fa-book" title="笔记本" rel="tooltip-bottom"></i> '+this.cn_notebook_name+'<button type="button" class="btn btn-default btn-xs btn_position btn_delete"><i class="fa fa-times"></i></button></a></li>';					
					//将li插入到笔记本列表ul中
					$("#first_side_right ul").append(li);
					//给li绑定笔记本数据，将来修改、
					//删除笔记本，或者根据笔记本查询
					//笔记时要使用这个绑定的数据
					$("#first_side_right ul li:last").data("notebook",this);
				});
			} else {
				//程序执行失败
				alert(result.message);
				//如果要没有登录，踢到登录页面
				if(result.message=="没有登录"){
					setTimeout(function(){
						location.href = "login.html";
					},2000);
				}
			}
		}
	);
}

/***
 * 加载特殊笔记本
 */
function loadSpecialNoteBook(){
	console.log("into loadSpecialNoteBook");
	$.post(
		basePath+"/notebook/findSpecial.do",
		{},
		function(result) {
			console.log("into callback function");
			if(result.success) {
				//程序执行成功
				//[{},{},{},{}]
				var list = result.data;
				$(list).each(function(){
					if(this.cn_notebook_type_id=='1') {
						//绑定到收藏
						$("#like_button").data("notebook",this);
					} else if (this.cn_notebook_type_id=='2') {
						//绑定到回收站
						$("#rollback_button").data("notebook",this);
					} else if (this.cn_notebook_type_id=='3') {
						//绑定到活动
						$("#action_button").data("notebook",this);
					} else {
						//绑定到默认(推送)
						$("#first_side_right ul li:first").data("notebook",this);
					}
				});
			} else {
				//程序执行失败
				alert(result.message);
			}
		}
	);
}

/****
 * 添加笔记本
 */
function addNoteBook(noteBookName){
	$.post(
		basePath+"/notebook/addNormal.do",
		{"noteBookName":noteBookName},
		function(result) {
			if(result.success) {
				//程序执行成功
				//关闭弹出框
				$('.close,.cancle').trigger('click');
		        //在ul中插入笔记本
		        var notebook = result.data;
		        var li = '<li class="online"><a class="unchecked"><i class="fa fa-book" title="笔记本" rel="tooltip-bottom"></i> '+notebook.cn_notebook_name+'<button type="button" class="btn btn-default btn-xs btn_position btn_delete"><i class="fa fa-times"></i></button></a></li>';
		        $("#first_side_right ul").append(li);
		        $("#first_side_right ul li:last").data("notebook",notebook);
			} else {
				//程序执行失败
				alert(result.message);
			}
		}
	);
}

/***
 * 重命名笔记本
 */
function updateNoteBook(newName,notebook,checked_li){
	console.log("into updateNoteBook");
	console.log(notebook.cn_notebook_createtime);
	
	notebook.cn_notebook_name=newName;
	$.post(
		basePath+"/notebook/update.do",	
		notebook,
		function(result) {
			if(result.success) {
				//成功
				//关闭弹出框
				$(".close,.cancle").trigger("click");
				//将li中的笔记本名换成新名称
				var a = '<a class="checked"><i class="fa fa-book" title="笔记本" rel="tooltip-bottom"></i> '+newName+'<button type="button" class="btn btn-default btn-xs btn_position btn_delete"><i class="fa fa-times"></i></button></a>';
				checked_li.html(a);
				//将修改后的数据绑定到li
				checked_li.data("notebook",notebook);
			} else {
				//失败
				alert(result.message);
			}
		}
	);
}

/***
 * 删除笔记本
 */
function deleteNoteBook(checked_li){
	//判断笔记本是否包含笔记
	//在完成笔记查询功能后补充
	var noteSize = $("#second_side_right ul li").length;
	if(noteSize>0) {
		alert("该笔记本下存在笔记.");
		return;
	}
	//删除笔记本
	var notebook = checked_li.data("notebook");
	$.post(
		basePath+"/notebook/delete.do",
		{"id":notebook.cn_notebook_id},
		function(result) {
			if(result.success) {
				//程序执行成功
				//移除当前选中的笔记本li
				checked_li.remove();
				//关闭提示框
				$(".close,.cancle").trigger("click");
			} else {
				//程序执行失败
				alert(result.message);
			}
		}
	);
}

/**
 * 将笔记本列表放置到select组件中
 */
function setNoteBookToSelect(){
	$.post(
		basePath+"/notebook/findNormal.do",	
		{},
		function(result) {
			if(result.success) {
				//将默认笔记本加入select
				var li = $("#first_side_right ul li:first");
				var notebook = li.data("notebook");
				var p = '<option value="'+notebook.cn_notebook_id+'">默认笔记本</option>';
				$("#moveSelect").append(p);
				//取得该用户所有的普通笔记本
				var list = result.data;
				//遍历该数组，将每一个笔记本加入下拉选
				$(list).each(function(){
					//创建option
					//下拉选是表单元素，可以直接调用val方法取值，
					//该方法会自动返回你选中的option的value
					var option = '<option value="'+this.cn_notebook_id+'">'+this.cn_notebook_name+'</option>';
					//加入select
					$("#moveSelect").append(option);
				});
			} else {
				alert(result.message);
			}
		}
	);
}
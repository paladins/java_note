package com.tarena.dao;

import java.util.List;

import com.tarena.entity.NoteBook;

@MyBatisRepository
public interface NoteBookMapper {

	/**
	 * 插入一条笔记本
	 */
	void save(NoteBook noteBook);
	
	/**
	 * 查询某用户所有的普通笔记本
	 */
	List<NoteBook> findNormal(String userId);
	
	/**
	 * 查询某用户所有的特殊笔记本
	 */
	List<NoteBook> findSpecial(String userId);
	
	/**
	 * 修改笔记本
	 */
	void update(NoteBook noteBook);
	
	/**
	 * 删除笔记本
	 */
	void delete(String id);
	
}

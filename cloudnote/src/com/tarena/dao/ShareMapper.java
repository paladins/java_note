package com.tarena.dao;

import java.util.List;
import java.util.Map;

import com.tarena.entity.Share;

@MyBatisRepository
public interface ShareMapper {

	void save(Share share);
	
	List<Share> findByNoteId(String noteId);
	
	void update(Share share);
	
	/**
	 * 分页查询分享笔记，Map参数中包含3个值：
	 * 	key						value
	 * 	searchKey			xxx
	 * begin					1
	 * pageSize				5		
	 */
	List<Share> findByPage(Map<String, Object> param);
	
}

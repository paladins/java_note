package com.tarena.dao;

import com.tarena.entity.User;

@MyBatisRepository
public interface UserMapper {

	/**
	 * 根据用户名查询用户
	 */
	User findByName(String userName);
	
	/**
	 * 插入一个用户
	 */
	void save(User user);
	
	/**
	 * 修改用户：
	 * 可以支持修改密码；
	 * 也可以支持修改其他的字段；
	 * 主要是从方法的复用性考虑。
	 */
	void update(User user);
	
}

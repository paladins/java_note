package com.tarena.service;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.tarena.dao.NoteBookMapper;
import com.tarena.dao.UserMapper;
import com.tarena.entity.NoteBook;
import com.tarena.entity.User;
import com.tarena.util.Md5Util;
import com.tarena.util.SystemConstant;

/**
 *	登录模块的业务组件。
 *	将来要使用Spring声明式事务来管理该
 *	业务组件的事务，声明式事务很简单，
 *	只需要配置即可，但要求这些业务方法
 *	具有一定的规律，以便于事务管理组件可以
 *	识别该方法做了增删改查处理中的哪一种。
 */
@Service
public class LoginService implements SystemConstant{
	
	@Resource
	private UserMapper userMapper;
	
	@Resource
	private NoteBookMapper noteBookMapper;
	
	/**
	 * 添加用户
	 */
	public boolean addUser(User user) {
		if(user == null) {
			throw new RuntimeException(
					"参数不能为空.");
		}
		//1.判断用户名是否存在
		User u = userMapper.findByName(
				user.getCn_user_name());
		if(u == null) {
			//用户名不存在，可以创建用户
			//2.新增用户
			createUser(user);
			//3.给用户初始化笔记本
			initNoteBook(user.getCn_user_id());
			return true;
		} else {
			//用户名已存在
			return false;
		}
	}

	public void initNoteBook(String userId) {
		//初始化收藏笔记本
		NoteBook n1 = new NoteBook();
		n1.setCn_notebook_id(
				UUID.randomUUID().toString());
		n1.setCn_notebook_type_id(
				FAVORITES_TYPE_ID);
		n1.setCn_notebook_name(
				FAVORITES_TYPE_NAME);
		n1.setCn_user_id(userId);
		n1.setCn_notebook_createtime(
				new Timestamp(System.currentTimeMillis()));
		noteBookMapper.save(n1);
		//初始化回收站笔记本
		NoteBook n2 = new NoteBook();
		n2.setCn_notebook_id(
				UUID.randomUUID().toString());
		n2.setCn_notebook_type_id(
				RECYCLE_TYPE_ID);
		n2.setCn_notebook_name(
				RECYCLE_TYPE_NAME);
		n2.setCn_user_id(userId);
		n2.setCn_notebook_createtime(
				new Timestamp(System.currentTimeMillis()));
		noteBookMapper.save(n2);
		//初始化活动笔记本
		NoteBook n3 = new NoteBook();
		n3.setCn_notebook_id(
				UUID.randomUUID().toString());
		n3.setCn_notebook_type_id(
				ACTION_TYPE_ID);
		n3.setCn_notebook_name(
				ACTION_TYPE_NAME);
		n3.setCn_user_id(userId);
		n3.setCn_notebook_createtime(
				new Timestamp(System.currentTimeMillis()));
		noteBookMapper.save(n3);
		//初始化推送(默认)笔记本
		NoteBook n4 = new NoteBook();
		n4.setCn_notebook_id(
				UUID.randomUUID().toString());
		n4.setCn_notebook_type_id(
				PUSH_TYPE_ID);
		n4.setCn_notebook_name(
				PUSH_TYPE_NAME);
		n4.setCn_user_id(userId);
		n4.setCn_notebook_createtime(
				new Timestamp(System.currentTimeMillis()));
		noteBookMapper.save(n4);
	}

	public void createUser(User user) {
		user.setCn_user_id(
			UUID.randomUUID().toString());
		String md5Password = 
			Md5Util.md5(user.getCn_user_password());
		user.setCn_user_password(md5Password);
		userMapper.save(user);
	}

	/**
	 * 校验账号和密码是否正确
	 */
	public Map<String, Object> checkUser(
			String userName, String password) {
		if(userName == null)
			throw new RuntimeException(
					"用户名不能为空.");
		if(password  == null)
			throw new RuntimeException(
					"密码不能为空.");
		Map<String, Object> map = 
			new HashMap<String, Object>();
		
		//校验用户名是否正确
		User u = userMapper.findByName(userName);
		if(u == null) {
			//用户名不存在
			map.put("flag", USER_NAME_ERROR);
			return map;
		} 
		
		//需要对加密后的密码进行比较
		String md5Password = Md5Util.md5(password);
		if(!md5Password.equals(
				u.getCn_user_password())) {
			map.put("flag", PASSWORD_ERROR);
			return map;
		}
		
		//校验成功
		map.put("flag", SUCCESS);
		map.put("user", u);
		return map;
	}
	
	/**
	 * 修改密码
	 * @param oldPassword
	 * 		用户输入的原密码
	 * @param newPassword
	 * 		用户输入的新密码
	 * @param user
	 * 		session中存储的用户，它是和数据库
	 * 		中的用户一致的，没必要再查询了。
	 * @return
	 */
	public boolean changePassword(
			String oldPassword,
			String newPassword,
			User user) {
		if(oldPassword == null
				|| newPassword == null
				|| user == null) {
			throw new RuntimeException(
					"参数不能为空.");
		}
		//校验旧密码
		String md5OldPassword = 
			Md5Util.md5(oldPassword);
		if(md5OldPassword.equals(
				user.getCn_user_password())) {
			//旧密码正确，修改密码
			user.setCn_user_password(
					Md5Util.md5(newPassword));
			userMapper.update(user);
			return true;
		} else {
			//旧密码错误
			return false;
		}
	}
	
}

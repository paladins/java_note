package com.tarena.service;

import java.sql.Timestamp;
import java.util.List;
import java.util.UUID;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import com.tarena.dao.NoteBookMapper;
import com.tarena.entity.NoteBook;

/**
 *	笔记本模块的业务组件
 */
@Service
public class NoteBookService {
	
	private Logger logger = 
		Logger.getLogger(NoteBookService.class);

	@Resource
	private NoteBookMapper noteBookMapper;
	
	/**
	 * 查询某用户所有的普通笔记本
	 */
	public List<NoteBook> findNormal(String userId) {
		logger.debug("into findNormal");
		if(userId == null) {
			throw new RuntimeException(
					"参数不能为空.");
		}
		List<NoteBook> list = 
			noteBookMapper.findNormal(userId);
		logger.info(list == null ? "" : list.size());
		logger.info("out findNormal");
		return list;
	}
	
	/**
	 * 查询指定用户的所有特殊的笔记本
	 */
	public List<NoteBook> findSpecial(String userId) {
		logger.info("into findSpecial");
		if(userId == null)
			throw new RuntimeException("参数不能为空.");
		List<NoteBook> list = 
			noteBookMapper.findSpecial(userId);
		logger.info("out findSpecial");
		return list;
	}
	
	/**
	 * 新增笔记本
	 */
	public NoteBook add(String userId,
			String noteBookName,
			String noteBookTypeId) {
		if(userId==null
				|| noteBookName==null
				|| noteBookTypeId==null)
			throw new RuntimeException(
					"参数不能为空.");
		
		NoteBook n = new NoteBook();
		n.setCn_notebook_id(
			UUID.randomUUID().toString());
		n.setCn_user_id(userId);
		n.setCn_notebook_type_id(noteBookTypeId);
		n.setCn_notebook_name(noteBookName);
		n.setCn_notebook_createtime(
				new Timestamp(System.currentTimeMillis()));
		noteBookMapper.save(n);
		return n;
	}
	
	/**
	 * 修改笔记本
	 */
	public void update(NoteBook noteBook) {
		if(noteBook==null)
			throw new RuntimeException(
					"参数为空.");
		noteBookMapper.update(noteBook);
	}
	
	/**
	 * 删除笔记本
	 */
	public void delete(String id) {
		if(id==null)
			throw new RuntimeException("参数为空.");
		noteBookMapper.delete(id);
	}
	
}

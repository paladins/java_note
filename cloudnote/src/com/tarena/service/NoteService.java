package com.tarena.service;

import java.util.List;
import java.util.UUID;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.tarena.dao.NoteMapper;
import com.tarena.dao.ShareMapper;
import com.tarena.entity.Note;
import com.tarena.entity.Share;

@Service
public class NoteService {

	@Resource
	private NoteMapper noteMapper;
	
	@Resource
	private ShareMapper shareMapper;
	
	/**
	 * 查询某笔记本下的所有笔记
	 */
	public List<Note> findByNoteBookId(String noteBookId) {
		if(noteBookId == null)
			throw new RuntimeException("参数为空.");
		return noteMapper.findByNoteBookId(noteBookId);
	}
	
	/**
	 * 新增笔记
	 */
	public Note addNote(String noteBookId,
			String noteTitle, String userId) {
		if(noteBookId==null
				|| noteTitle==null
				|| userId==null) {
			throw new RuntimeException("参数为空.");
		}
		
		Note note = new Note();
		note.setCn_note_id(
				UUID.randomUUID().toString());
		note.setCn_notebook_id(noteBookId);
		note.setCn_user_id(userId);
		note.setCn_note_title(noteTitle);
		note.setCn_note_create_time(
				System.currentTimeMillis());
		note.setCn_note_last_modify_time(
				System.currentTimeMillis());
		noteMapper.save(note);
		return note;
	}
	
	/**
	 * 将笔记移动到指定的笔记本下
	 */
	public void move(String noteId, String noteBookId) {
		if(noteId == null
				|| noteBookId==null)
			throw new RuntimeException("参数为空");
		Note note = noteMapper.findById(noteId);
		note.setCn_notebook_id(noteBookId);
		noteMapper.update(note);
	}
	
	/**
	 * 修改笔记
	 */
	public void update(Note note) {
		note.setCn_note_last_modify_time(
				System.currentTimeMillis());
		noteMapper.update(note);
	}
	
	/**
	 * 分享笔记
	 */
	public void share(Note note) {
		if(note==null)
			throw new RuntimeException("参数为空.");
		//查询当前note是否存在分享数据
		List<Share> shares = 
			shareMapper.findByNoteId(
					note.getCn_note_id());
		if(shares==null||shares.size()==0) {
			//没有分享过，需要新创建数据
			Share share = new Share();
			share.setCn_share_id(
					UUID.randomUUID().toString());
			share.setCn_share_title(
					note.getCn_note_title());
			share.setCn_share_body(
					note.getCn_note_body());
			share.setCn_note_id(note.getCn_note_id());
			shareMapper.save(share);
		} else {
			//已经分享过，覆盖
			for(Share s : shares) {
				s.setCn_share_title(note.getCn_note_title());
				s.setCn_share_body(note.getCn_note_body());
				shareMapper.update(s);
			}
		}
	}
	
}

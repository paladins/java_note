package com.tarena.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.tarena.dao.NoteBookMapper;
import com.tarena.dao.NoteMapper;
import com.tarena.dao.ShareMapper;
import com.tarena.entity.Note;
import com.tarena.entity.NoteBook;
import com.tarena.entity.Share;
import com.tarena.util.SystemConstant;

@Service
public class ShareService implements SystemConstant {

	@Resource
	private ShareMapper shareMapper;
	
	@Resource
	private NoteBookMapper noteBookMapper;
	
	@Resource
	private NoteMapper noteMapper;
	
	/**
	 * 搜索分享笔记
	 */
	public List<Share> searchShares(
			String searchKey, int currentPage) {
		if(searchKey == null
				|| searchKey.length() == 0) {
			return null;
		}
		
		//计算当前页的起始行
		int begin = (currentPage-1)*PAGE_SIZE;
		Map<String, Object> param = 
			new HashMap<String, Object>();
		param.put("searchKey", searchKey);
		param.put("begin", begin);
		param.put("pageSize", PAGE_SIZE);
		
		return shareMapper.findByPage(param);
	}
	
	/**
	 * 收藏分享笔记
	 */
	public void like(Share share, String userId) {
		if(share==null
				|| userId==null)
			throw new RuntimeException("参数为空.");
		Note note = new Note();
		note.setCn_note_id(
				UUID.randomUUID().toString());
		note.setCn_note_title(share.getCn_share_title());
		note.setCn_note_body(share.getCn_share_body());
		note.setCn_user_id(userId);
		note.setCn_note_create_time(
				System.currentTimeMillis());
		note.setCn_note_last_modify_time(
				System.currentTimeMillis());
		
		List<NoteBook> list = 
			noteBookMapper.findSpecial(userId);
		for(NoteBook n : list) {
			if(n.getCn_notebook_type_id().equals(FAVORITES_TYPE_ID)) {
				note.setCn_notebook_id(n.getCn_notebook_id());
				break;
			}
		}
		
		noteMapper.save(note);
	}
	
}

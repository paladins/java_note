package com.tarena.test;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.tarena.dao.NoteBookMapper;
import com.tarena.dao.NoteMapper;
import com.tarena.dao.ShareMapper;
import com.tarena.dao.UserMapper;
import com.tarena.entity.Note;
import com.tarena.entity.NoteBook;
import com.tarena.entity.Share;
import com.tarena.entity.User;

public class TestMapper {
	
	private ApplicationContext ctx = 
		new ClassPathXmlApplicationContext(
				"config/applicationContext.xml");
	
	/**
	 * ShareMapper.findByPage
	 */
	@Test
	public void test13() {
		ShareMapper mapper = 
			ctx.getBean(ShareMapper.class);
		Map<String, Object> param = 
			new HashMap<String, Object>();
		param.put("searchKey", "1");
		param.put("begin", 10);
		param.put("pageSize", 5);
		List<Share> list = 
			mapper.findByPage(param);
		for(Share s : list) {
			System.out.println(
				s.getCn_share_title() 
			);
		}
	}
	
	/**
	 * ShareMapper.findByNoteId
	 * ShareMapper.update
	 */
	@Test
	public void test12() {
		ShareMapper mapper = 
			ctx.getBean(ShareMapper.class);
		List<Share> list = 
			mapper.findByNoteId("53d1b3ed-59a1-4715-a7b2-9027b0d551e0");
		for(Share s : list) {
			System.out.println(
				s.getCn_share_title() + " " +
				s.getCn_share_body()
			);
			s.setCn_share_title("jsd1410");
			s.setCn_share_body("JSD1410");
			mapper.update(s);
		}
	}
	
	/**
	 * ShareMapper.save
	 */
	@Test
	public void test11() {
		ShareMapper mapper = 
			ctx.getBean(ShareMapper.class);
		Share s = new Share();
		s.setCn_share_id(UUID.randomUUID().toString());
		s.setCn_share_title("jsd1410");
		s.setCn_share_body("JSD1410");
		s.setCn_note_id("y1");
		mapper.save(s);
	}
	
	@Test
	public void test10() {
		NoteMapper mapper = 
			ctx.getBean(NoteMapper.class);
		//模拟要修改的数据
		Note note = new Note();
		note.setCn_note_id("2bc5b4f4-cf0b-48e9-aec5-37c43288409a");
		note.setCn_notebook_id("a3");
		note.setCn_user_id("0b765ac0-408b-4ad3-9ee5-5453b7217b1a");
		note.setCn_note_title("yyy");
		note.setCn_note_create_time(new Long("1423553720468"));
		note.setCn_note_last_modify_time(new Long("1423553720468"));
		
		//模拟修改
		note.setCn_notebook_id("9581b6b8-4a73-477f-84d6-6c986ddb7edd");
		note.setCn_note_body("YYY");
		
		mapper.update(note);
	}
	
	/**
	 * NoteMapper.save
	 */
	@Test
	public void test9() {
		NoteMapper mapper = 
			ctx.getBean(NoteMapper.class);
		Note n = new Note();
		n.setCn_note_id(UUID.randomUUID().toString());
		n.setCn_notebook_id("9581b6b8-4a73-477f-84d6-6c986ddb7edd");
		n.setCn_user_id("0b765ac0-408b-4ad3-9ee5-5453b7217b1a");
		n.setCn_note_title("SpringMVC");
		n.setCn_note_create_time(
				System.currentTimeMillis());
		n.setCn_note_last_modify_time(
				System.currentTimeMillis());
		mapper.save(n);
	}
	
	/**
	 * NoteMapper.findByNoteBookId
	 */
	@Test
	public void test8() {
		NoteMapper mapper = 
			ctx.getBean(NoteMapper.class);
		List<Note> list = 
			mapper.findByNoteBookId(
					"9581b6b8-4a73-477f-84d6-6c986ddb7edd");
		for(Note n : list) {
			System.out.println(
				n.getCn_note_id() + " " +
				n.getCn_note_title() + " " +
				n.getCn_note_body()
			);
		}
	}
	
	/**
	 * NoteBookMapper.update
	 */
	@Test
	public void test7() {
		NoteBookMapper mapper = 
			ctx.getBean(NoteBookMapper.class);
		//假设该数据是查出来的
		NoteBook n = new NoteBook();
		n.setCn_notebook_id(
			"deee5903-9481-4e0b-87bb-31983a9bad13");
		n.setCn_notebook_name("云笔记项目");
		n.setCn_notebook_type_id("5");
		n.setCn_user_id(
				"0b765ac0-408b-4ad3-9ee5-5453b7217b1a");
		//模拟修改行为
		n.setCn_notebook_name("cloud note");
		mapper.update(n);
	}
	
	/**
	 * NoteBookMapper.findSpecial
	 */
	@Test
	public void test6() {
		NoteBookMapper mapper = 
			ctx.getBean(NoteBookMapper.class);
		List<NoteBook> list = 
			mapper.findSpecial(
					"0b765ac0-408b-4ad3-9ee5-5453b7217b1a");
		for(NoteBook n : list) {
			System.out.println(
				n.getCn_notebook_id() + " " +
				n.getCn_notebook_name()
			);
		}
	}
	
	/**
	 * NoteBookMapper.findNormal
	 */
	@Test
	public void test5() {
		NoteBookMapper mapper = 
			ctx.getBean(NoteBookMapper.class);
		List<NoteBook> list = mapper.findNormal(
				"0b765ac0-408b-4ad3-9ee5-5453b7217b1a");
		for(NoteBook n : list) {
			System.out.println(
				n.getCn_notebook_id() + " " +
				n.getCn_notebook_name()
			);
		}
	}
	
	/**
	 * UserMapper.update
	 */
	@Test
	public void test4() {
		UserMapper mapper = 
			ctx.getBean(UserMapper.class);
		User user = 
			mapper.findByName("zhangsan");
		user.setCn_user_password("abc");
		user.setCn_user_desc("张小三");
		mapper.update(user);
	}
	
	/**
	 * NoteBookMapper.save()
	 */
	@Test
	public void test3() {
		NoteBookMapper mapper = 
			ctx.getBean(NoteBookMapper.class);
		NoteBook n = new NoteBook();
		n.setCn_notebook_id(
				UUID.randomUUID().toString());
		n.setCn_user_id(
			"52f9b276-38ee-447f-a3aa-0d54e7a736e4");
		n.setCn_notebook_type_id("5");
		n.setCn_notebook_name("云笔记项目开发");
		n.setCn_notebook_createtime(
				new Timestamp(System.currentTimeMillis()));
		mapper.save(n);
				
	}
	
	/**
	 * UserMapper.save()
	 */
	@Test
	public void test2() {
		UserMapper mapper = 
			ctx.getBean(UserMapper.class);
		User u = new User();
		u.setCn_user_id(UUID.randomUUID().toString());
		u.setCn_user_name("zhangsan");
		u.setCn_user_password("123");
		u.setCn_user_desc("张三");
		mapper.save(u);
	}
	
	/**
	 * UserMapper.findByName()
	 */
	@Test
	public void test1() {
		UserMapper mapper = 
			ctx.getBean(UserMapper.class);
		User user = mapper.findByName("demo");
//		Integer.valueOf("abc");
		System.out.println(user.getCn_user_id());
		System.out.println(user.getCn_user_name());
		System.out.println(user.getCn_user_password());
	}

}

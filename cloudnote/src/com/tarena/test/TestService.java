package com.tarena.test;

import java.util.List;
import java.util.Map;

import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.tarena.dao.UserMapper;
import com.tarena.entity.NoteBook;
import com.tarena.entity.User;
import com.tarena.service.LoginService;
import com.tarena.service.NoteBookService;
import com.tarena.service.NoteService;

public class TestService {
	
	private ApplicationContext ctx = 
		new ClassPathXmlApplicationContext(
				"config/applicationContext.xml");
	
	/**
	 * NoteService.move
	 */
	@Test
	public void test7() {
		NoteService ser = 
			ctx.getBean(NoteService.class);
		ser.move("2bc5b4f4-cf0b-48e9-aec5-37c43288409a", "a3");
	}
	
	/**
	 * NoteBookService.delete
	 */
	@Test
	public void test6() {
		NoteBookService ser = 
			ctx.getBean(NoteBookService.class);
		ser.delete("deee5903-9481-4e0b-87bb-31983a9bad13");
	}
	
	/**
	 * NoteBookService.add
	 */
	@Test
	public void test5() {
		NoteBookService ser = 
			ctx.getBean(NoteBookService.class);
		ser.add("0b765ac0-408b-4ad3-9ee5-5453b7217b1a", 
					"WEB基础", "5");
	}
	
	/**
	 * NoteBookService.findNormal
	 */
	@Test
	public void test4() {
		NoteBookService ser = 
			ctx.getBean(NoteBookService.class);
		List<NoteBook> list = 
			ser.findNormal("0b765ac0-408b-4ad3-9ee5-5453b7217b1a");
		for(NoteBook n : list) {
			System.out.println(
				n.getCn_notebook_id() + " " + n.getCn_notebook_name());
		}
	}
	
	/**
	 * LoginService.changePassword()
	 */
	@Test
	public void test3() {
		LoginService ser = 
			ctx.getBean(LoginService.class);
		UserMapper mapper = 
			ctx.getBean(UserMapper.class);
		User user  = mapper.findByName("ccc");
		boolean b = 
			ser.changePassword("cccccc", "111111", user);
		System.out.println(b);
		//c1f68ec06b490b3ecb4066b1b13a9ee9
	}
	
	/**
	 * LoginService.checkUser()
	 */
	@Test
	public void test2() {
		LoginService ser = 
			ctx.getBean(LoginService.class);
		Map<String, Object> map = ser.checkUser("liubei", "123456");
		System.out.println(map.get("flag"));
	}
	
	/**
	 * LoginService.addUser
	 */
	@Test
	public void test1() {
		LoginService ser = 
			ctx.getBean(LoginService.class);
		User u = new User();
		u.setCn_user_name("liubei");
		u.setCn_user_password("123456");
		u.setCn_user_desc("刘备");
		ser.addUser(u);
	}

}

package com.tarena.util;

/**
 *	系统常量接口
 */
public interface SystemConstant {

	//笔记本类型ID
	String FAVORITES_TYPE_ID = "1";
	String RECYCLE_TYPE_ID = "2";
	String ACTION_TYPE_ID="3";
	String PUSH_TYPE_ID="4";
	String NORMAL_TYPE_ID="5";
	
	//笔记本类型名
	String FAVORITES_TYPE_NAME="收藏";
	String RECYCLE_TYPE_NAME="回收站";
	String ACTION_TYPE_NAME="活动";
	String PUSH_TYPE_NAME="推送";
	String NORMAL_TYPE_NAME="正常";
	
	//登录校验结果
	int SUCCESS = 0;
	int USER_NAME_ERROR = 1;
	int PASSWORD_ERROR = 2;
	
	//分页时每页显示的最大行数
	int PAGE_SIZE = 5;
	
}

package com.tarena.web;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.tarena.entity.Result;
import com.tarena.entity.User;
import com.tarena.service.LoginService;
import com.tarena.util.SystemConstant;

/**
 *	登录模块
 */
@Controller
@RequestMapping("/login")
public class LoginController {
	
	@Resource
	private LoginService loginService;

	/**
	 * 	页面传入的参数，SpringMVC会自动接收，
	 * 	接收的时机是在调用方法之前。
	 * 实际上SpringMVC仅仅是采用了傻瓜式的
	 * 	处理方式进行判断，它会遍历该方法的
	 * 所有参数，然后判断页面传入的参数是否
	 * 与该参数匹配，若匹配则赋值，否则不做
	 * 	任何处理。
	 * 匹配规则：
	 * 		如果是实体或Map，SpringMVC会将参数
	 * 		与实体属性或Map的可以匹配；
	 * 		如果是基本类型，SpringMVC会直接按照
	 * 		参数名匹配；
	 */
	@RequestMapping("/register.do")
	@ResponseBody
	public Result register(User user) {
		System.out.println("into register");
		boolean b = loginService.addUser(user);
		System.out.println("out register");
		//{"success":true,"message":"","data":true}
		return new Result(b);
	}
	
	@RequestMapping("/login.do")
	@ResponseBody
	public Result login(String userName,
			String password, HttpSession session) {
		//校验用户名和密码
		Map<String, Object> map = 
			loginService.checkUser(userName, password);
		int flag = Integer.valueOf(
				map.get("flag") .toString());
		if(flag == SystemConstant.SUCCESS) {
			//登录成功，将登录信息存入session
			User user = (User) map.get("user");
			session.setAttribute("user", user);
			
			Map<String, Object> m = 
				new HashMap<String, Object>();
			m.put("flag", flag);
			m.put("userId", user.getCn_user_id());
			m.put("userName", user.getCn_user_name());
			//{"success":true,"message":null,"data":{"flag":0,"userId":"xxx","userName":"aaa"}}
			return new Result(m);
		} else {
			//登录失败
			Map<String, Object> m = 
				new HashMap<String, Object>();
			m.put("flag", flag);
			//{"success":true,"message":null,"data":{"flag":1}}
			return new Result(m);
		}
	}
	
	/**
	 * 退出
	 */
	@RequestMapping("/logout.do")
	@ResponseBody
	public Result logout(HttpSession session) {
		//注销session
		session.invalidate(); 
		return new Result();
	}
	
	@RequestMapping("/changePassword.do")
	@ResponseBody
	public Result changePassword(String oldPassword,
			String newPassword, HttpSession session) {
		User user = (User) session.getAttribute("user");
		boolean b = loginService.changePassword(
				oldPassword, newPassword, user);
		return new Result(b);
	}
	
}

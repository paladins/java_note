package com.tarena.web;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.support.WebBindingInitializer;
import org.springframework.web.context.request.WebRequest;

import java.sql.Timestamp;
import com.tarena.entity.NoteBook;
import com.tarena.entity.Result;
import com.tarena.entity.User;
import com.tarena.service.NoteBookService;
import com.tarena.util.SystemConstant;
import com.tarena.util.TimestampEditor;

@Controller
@RequestMapping("/notebook")
public class NoteBookController 
	implements SystemConstant,WebBindingInitializer {
	
	private Logger logger = 
		Logger.getLogger(NoteBookController.class);
	
	@Resource
	private NoteBookService noteBookService;
	
	@RequestMapping("/findNormal.do")
	@ResponseBody
	public Result findNormal(HttpSession session) {
		logger.info(
				"into NoteBookController.findNormal");
		//从session中获取当前的用户
		User user = (User)
			session.getAttribute("user");
		//查询当前用户所有的普通笔记本
		List<NoteBook> list = 
			noteBookService.findNormal(
					user.getCn_user_id());
		logger.info(
			"out NoteBookController.findNormal");
		//{"success":true,"message":null,"data":[{},{},{}]}
		return new Result(list);
	}
	
	@RequestMapping("/findSpecial.do")
	@ResponseBody
	public Result findSpecial(HttpSession session) {
		logger.info(
				"into NoteBookController.findSpecial");
		User user = (User)
			session.getAttribute("user");
		List<NoteBook> list = 
			noteBookService.findSpecial(
					user.getCn_user_id());
		logger.info(
			"out NoteBookController.findSpecial");
		//{"success":true,"message":null,"data":[{},{},{}]}
		return new Result(list);
	}
	
	@RequestMapping("/addNormal.do")
	@ResponseBody
	public Result addNormal(String noteBookName,
			HttpSession session) {
		User user = (User) session.getAttribute("user");
		NoteBook n = noteBookService.add(
				user.getCn_user_id(), noteBookName, 
				NORMAL_TYPE_ID);
		return new Result(n);
	}
	
	/**
	 * 参数noteBook用来接收页面传入的数据，
	 * 该操作是由SpringMVC自动处理的。
	 * 
	 * 实际上SpringMVC会将页面上所有的数据
	 * 看做字符串，然后将字符串数据设置给
	 * noteBook中的属性，若实体类中的属性不是
	 * 字符串，那么它需要将字符串数据自动
	 * 转型成属性类型再赋值。
	 * 
	 * SpringMVC在自动转型时，会调用不同的
	 * 类型转换器来处理转型，这些类型转换器
	 * 都继承于PropertyEditorSupport类，并覆写该
	 * 类中的setAsText方法实现类型转换。
	 */
	@RequestMapping("/update.do")
	@ResponseBody
	public Result update(NoteBook noteBook) {
		logger.info("into update");
		logger.info(noteBook.getCn_notebook_name());
		noteBookService.update(noteBook);
		logger.info("out update");
		return new Result();
	}
	
	@RequestMapping("/delete.do")
	@ResponseBody
	public Result delete(String id) {
		noteBookService.delete(id);
		return new Result();
	}

	@Override
	//告诉SpringMVC，在请求开始时先调用
	//当前的方法，然后再调用Controller处理
	//请求的方法。
	@InitBinder
	public void initBinder(
			WebDataBinder binder, WebRequest request) {
		logger.info("into initBinder");
		//将Timestamp类型的转换器注册为
		//TimestampEditor，之后SpringMVC在
		//处理Timestamp类型时会调用TimestampEditor
		//来进行类型转换
		binder.registerCustomEditor(
				Timestamp.class, new TimestampEditor());
	}

}

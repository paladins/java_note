package com.tarena.web;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.tarena.entity.Note;
import com.tarena.entity.Result;
import com.tarena.entity.User;
import com.tarena.service.NoteService;

@Controller
@RequestMapping("/note")
public class NoteController {

	@Resource
	private NoteService noteService;
	
	@RequestMapping("/findNote.do")
	@ResponseBody
	public Result findNote(String noteBookId) {
		List<Note> list = 
			noteService.findByNoteBookId(noteBookId);
		return new Result(list);
	}
	
	@RequestMapping("/add.do")
	@ResponseBody
	public Result add(String noteBookId,
			String noteTitle, HttpSession session) {
		User user = (User)
			session.getAttribute("user");
		Note note = noteService.addNote(noteBookId, 
				noteTitle, user.getCn_user_id());
		return new Result(note);
	}
	
	@RequestMapping("/move.do")
	@ResponseBody
	public Result move(String noteId, String noteBookId) {
		noteService.move(noteId, noteBookId);
		return new Result();
	}
	
	@RequestMapping("/update.do")
	@ResponseBody
	public Result update(Note note) {
		noteService.update(note);
		return new Result();
	}
	
	@RequestMapping("/share.do")
	@ResponseBody
	public Result share(Note note) {
		noteService.share(note);
		return new Result();
	}
	
}

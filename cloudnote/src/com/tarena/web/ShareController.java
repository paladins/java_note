package com.tarena.web;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.tarena.entity.Result;
import com.tarena.entity.Share;
import com.tarena.entity.User;
import com.tarena.service.ShareService;

@Controller
@RequestMapping("/share")
public class ShareController {

	@Resource
	private ShareService shareService;
	
	@RequestMapping("/search.do")
	@ResponseBody
	public Result search(String searchKey, int currentPage) {
		List<Share> list = 
			shareService.searchShares(
					searchKey, currentPage);
		return new Result(list);
	}
	
	@RequestMapping("/like.do")
	@ResponseBody
	public Result like(Share share, HttpSession session) {
		User user = (User)
			session.getAttribute("user");
		shareService.like(share, user.getCn_user_id());
		return new Result();
	}
	
}

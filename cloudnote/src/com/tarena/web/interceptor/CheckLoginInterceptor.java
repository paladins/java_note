package com.tarena.web.interceptor;

import java.io.Writer;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import com.tarena.entity.User;

/**
 *	登录检查拦截器
 */
public class CheckLoginInterceptor 
	implements HandlerInterceptor {

	//请求结束时调用
	@Override
	public void afterCompletion(HttpServletRequest arg0,
			HttpServletResponse arg1, Object arg2, Exception arg3)
			throws Exception {

	}

	//在执行完Controller方法后调用
	@Override
	public void postHandle(HttpServletRequest arg0, HttpServletResponse arg1,
			Object arg2, ModelAndView arg3) throws Exception {

	}

	//执行Controller方法前调用
	@Override
	public boolean preHandle(
			HttpServletRequest request, 
			HttpServletResponse response,
			Object obj) throws Exception {
		//获取session中的登录信息
		User user = (User) request
			.getSession().getAttribute("user");
		if(user == null) {
			//用户没有登录，不能继续访问
			String result = "{\"success\":false,\"message\":\"没有登录\"}";
			response.setCharacterEncoding("utf-8");
			response.setContentType("application/json");
			Writer writer = response.getWriter();
			writer.write(result);
			writer.close();
			return false;
		} else {
			//用户已登录，可以继续访问
			return true;
		}
	}

}
